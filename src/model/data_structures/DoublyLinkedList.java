package model.data_structures;

import java.util.Iterator;
import java.util.List;

public class DoublyLinkedList<T> implements IDoublyLinkedList<T> {
	
	private Nodo <T>primero;
	private Nodo <T> ultimo;
	private int tamanio=0;
	
	
	public DoublyLinkedList() {
		
				
	}

	public boolean agregar(T yo)
	{ 
		
		if (primero==null) {
			Nodo<T> elprimero = new Nodo<T>(null,null,yo);
			primero = elprimero;
			ultimo = elprimero;
		tamanio++;	
			return true;
		}else {
			Nodo<T> viejoUltimo=ultimo;
			Nodo<T> nuevoNodo = new Nodo<T>(null,viejoUltimo,yo);
			ultimo = nuevoNodo;
			viejoUltimo.siguiente=ultimo;
			tamanio++;
			return true;
			
		}	
	
	}
	
	public int getTamanio() {
		return tamanio;
	}


	public class Nodo <T>{
		private Nodo<T> siguiente;
		private Nodo<T> anterior;
		private T valor;
		public Nodo(Nodo<T> siguiente, Nodo<T> anterior, T yo) {
			
			this.siguiente = siguiente;
			this.anterior = anterior;
			this.valor = yo;
		}
		
	}


	@Override
	public Iterator<T> iterator() {
	
		return new Iterator<T>() {
			Nodo<T> actual=primero;
			@Override
			public boolean hasNext() {
				boolean hola=false;
				
				if (actual==null) {
					return hola;
				}
				if (actual.siguiente!=null) {
					hola=true;
				}
				return hola;
			}

			@Override
			public T next() {
				actual=actual.siguiente;
				return actual.valor;
			}
			
		};
	}

	@Override
	public Integer getSize() {
		
		return tamanio;
	}

}
