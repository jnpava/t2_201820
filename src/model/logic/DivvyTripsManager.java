package model.logic;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;

import api.IDivvyTripsManager;
import model.vo.VOByke;
import model.vo.VOStation;
import model.vo.VOTrip;
import model.data_structures.DoublyLinkedList;

public class DivvyTripsManager implements IDivvyTripsManager {

	public DoublyLinkedList<VOTrip> listaViajes = new DoublyLinkedList<VOTrip>();
	public void loadStations (String stationsFile) throws IOException {
		try {
			BufferedReader leer= new BufferedReader(new FileReader(stationsFile));
			leer.readLine();
			for(String s = leer.readLine(); s != null;s =leer.readLine()) {

				String[] datos = s.replaceAll(",,", ", , ").split(",");
				int idStation = Integer.parseInt(datos[0]);
				VOStation estacion= new VOStation(idStation);
			}
			leer.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// TODO Auto-generated method stub

		}


		public void loadTrips (String tripsFile){

			// TODO Auto-generated method stub
			try {
				BufferedReader leer= new BufferedReader(new FileReader(tripsFile));
				leer.readLine();
				for(String s = leer.readLine(); s != null;s =leer.readLine()) {

					String[] datos = s.replaceAll(",,", ", , ").split(",");
					int id = Integer.parseInt(datos[0]);
					int bikeId= Integer.parseInt(datos[3]);
					double getTripSeconds= Double.parseDouble(datos[4]);
					String getFromStation= datos[5];
					int getToStation= Integer.parseInt(datos[7]);
					System.out.println(getToStation+"vamos a ver si entra");
					String genero = datos[10];
					if (genero.equals(" ")) {
						genero = "ng";
						VOTrip viaje= new VOTrip(id, getTripSeconds, getFromStation, getToStation,genero);
						listaViajes.agregar(viaje);
					}
					else
					{
						genero= datos[10];
						VOTrip viaje= new VOTrip(id, getTripSeconds, getFromStation, getToStation,genero);
						listaViajes.agregar(viaje);
					}
					//VOByke cicla= new VOByke(bikeId);

				}
				leer.close();

			} catch (Exception e) {

				e.printStackTrace();
			}

		}

		@Override
		public DoublyLinkedList <VOTrip> getTripsOfGender (String gender) {
			DoublyLinkedList<VOTrip> listGenero=new DoublyLinkedList<VOTrip>();

			for(VOTrip v : listaViajes) {

				if(v.getGenero().equalsIgnoreCase(gender)) {
					listGenero.agregar(v);
				}
			}
			System.out.println(listGenero.getTamanio());
			return listGenero;
		}

		@Override
		public DoublyLinkedList <VOTrip> getTripsToStation (int stationID) {
			DoublyLinkedList<VOTrip> listIdStation=new DoublyLinkedList<VOTrip>();
			for(VOTrip v : listaViajes) {

				if(v.getToStation==stationID) {
					System.out.println(v.getToStation()+"este es");
					listIdStation.agregar(v);
				}
			}
			System.out.println(listIdStation.getTamanio());
			return listIdStation;
		}	

		public static void main(String[] args) {
			DivvyTripsManager s = new DivvyTripsManager();
			s.loadTrips("./data/Divvy_Trips_2017_Q4.csv");
			//System.out.println("hay tantos:");
			s.getTripsToStation(61);

		}


	}
