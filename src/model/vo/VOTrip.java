package model.vo;

/**
 * Representation of a Trip object
 */
public class VOTrip {

	private int id;
	private double getTripSeconds;
	private String getFromStation;
	public int getToStation;
	private String genero;
	
	
	
	
	
	public VOTrip(int id, double getTripSeconds, String getFromStation, int pGetToStation,String pGenero) {
		this.id = id;
		this.getTripSeconds = getTripSeconds;
		this.getFromStation = getFromStation;
		this.getToStation =pGetToStation;
		this.genero=pGenero;
		
	}


	/**
	 * @return id - Trip_id
	 */
	public int id() {
		// TODO Auto-generated method stub
		return 0;
	}	
	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public double getTripSeconds() {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * @return station_name - Origin Station Name .
	 */
	public String getFromStation() {
		// TODO Auto-generated method stub
		return "";
	}
	
	/**
	 * @return station_name - Destination Station Name .
	 */
	public int getToStation() {
		// TODO Auto-generated method stub
		return 0;
	}


	public String getGenero() {
		return genero;
	}


	public void setGenero(String genero) {
		this.genero = genero;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((genero == null) ? 0 : genero.hashCode());
		result = prime * result + ((getFromStation == null) ? 0 : getFromStation.hashCode());
		result = prime * result + getToStation;
		long temp;
		temp = Double.doubleToLongBits(getTripSeconds);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + id;
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VOTrip other = (VOTrip) obj;
		if (genero == null) {
			if (other.genero != null)
				return false;
		} else if (!genero.equals(other.genero))
			return false;
		if (getFromStation == null) {
			if (other.getFromStation != null)
				return false;
		} else if (!getFromStation.equals(other.getFromStation))
			return false;
		if (getToStation != other.getToStation)
			return false;
		if (Double.doubleToLongBits(getTripSeconds) != Double.doubleToLongBits(other.getTripSeconds))
			return false;
		if (id != other.id)
			return false;
		return true;
	}






}
