package api;

import java.io.IOException;

import model.data_structures.DoublyLinkedList;
import model.vo.VOTrip;

/**
 * Basic API for testing the functionality of the STS manager
 */
public interface IDivvyTripsManager {

	/**
	 * Method to load the Divvy trips of the STS
	 * @param tripsFile - path to the file 
	 */
	void loadTrips(String tripsFile);
	
	
	/**
	 * Method to load the Divvy Stations of the STS
	 * @param stationsFile - path to the file 
	 * @throws IOException 
	 */
	void loadStations(String stationsFile) throws IOException;
	

	public DoublyLinkedList <VOTrip> getTripsOfGender (String gender);
	
	
	public DoublyLinkedList <VOTrip> getTripsToStation (int stationID);


	
}
